# Resumer UI

A UI for the Resumer API created with React, Redux.

## Created with:

- React
- Redux
- Axios
  - AJAX requests to the Resumer API
- React Router
  - Client side routing
- jwt-decode
  - Decoding JSON Web Tokens
- [classnames](https://www.npmjs.com/package/classnames)
  - Conditional jsx classNames
- [ag-Grid](https://www.ag-grid.com)
  - Interactive data table
- [UIKit](https://getuikit.com/)
  - UI framework
- [SpinKit](http://tobiasahlin.com/spinkit/)
  - Simple CSS spinners

## TODO:

- visualize statistics based on user data
